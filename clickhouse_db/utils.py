MAPPING_TYPE = {'object': 'String',
                'uint64': 'UInt64',
                'uint32': 'UInt32',
                'uint16': 'UInt16',
                'uint8': 'UInt8',
                'float64': 'Float64',
                'float32': 'Float32',
                'int64': 'Int64',
                'int32': 'Int32',
                'int16': 'Int16',
                'int8': 'Int8',
                'datetime64[D]': 'Date',
                'datetime64[ns]': 'DateTime'}

REVERSE_MAPPING_TYPE = dict(zip(MAPPING_TYPE.values(), MAPPING_TYPE.keys()))

SPECIAL_CHARS = {
    '\b': '\\b',
    '\f': '\\f',
    '\r': '\\r',
    '\n': '\\n',
    '\t': '\\t',
    '\0': '\\0',
    '\\': '\\\\',
    "'": "\\'"
}


def escape(value, quote='`'):
    if not isinstance(value, (str, bytes)):
        return value
    value = ''.join(SPECIAL_CHARS.get(c, c) for c in value)
    if quote == '`':
        return '`{}`'.format(value)
    elif quote == '\'':
        return '\'{}\''.format(value)
    else:
        return value


def convert_types(types: list, to_numpy=False):
    """
    Converts the type of numpy to clickhouse or conversely
    :param to_numpy:
    :param types:
    :return:
    """
    if to_numpy:
        return list(map(REVERSE_MAPPING_TYPE.get, types))
    else:
        return list(map(MAPPING_TYPE.get, types))
