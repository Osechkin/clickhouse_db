import setuptools
import shutil
import glob


class CleanCommand(setuptools.Command):     # python setup.py clean
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        for pattern in ['./build', './dist', './*.pyc', './*.tgz', './*.egg-info']:
            for path in glob.glob(pattern):
                shutil.rmtree(path, ignore_errors=True)


setuptools.setup(
    cmdclass={
        'clean': CleanCommand,
    },
    name="clickhouse_db",
    version="0.0.5",
    author="Sergey Osechkin",
    author_email="osechkin.s.v@gmail.com",
    install_requires=["clickhouse_driver==0.1.2", "pandas>=0.25.1"],
    description="Methods for working with DB Clickhouse",
    packages=setuptools.find_packages(),
    python_requires='>=3.7',
    url="https://bitbucket.org/osechkin-SV/clickhouse_db/"
)
